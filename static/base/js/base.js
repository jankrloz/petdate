(function ($) {
    $(function () {

        if (!$('nav').hasClass('white')) {
            $('img#logo').attr('src', 'https://petdate.s3.amazonaws.com/static/base/img/logo-blanco.png');
        }

        $('.tooltipped').tooltip({delay: 50});
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $('select').material_select();
        $('.modal-trigger').leanModal();

    }); // end of document ready
})(jQuery); // end of jQuery name space

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$.validator.addMethod("greaterThan", function (value, element, params) {
    return value > $(params).val();
}, jQuery.validator.format('Debe ser mayor que {0}.'));

function loadingAnim(text) {
    return $('<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' + text + '<span class="sr-only">Loading...</span>');
}

// Setup ajax to include csrf token cookie on every ajax request http header
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

jQuery.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function addSpinner($element) {
    var spinner_html = $('<i class="fa fa-spinner fa-spin left fa-spin-btn fa-fw"></i>');
    $element.prepend(spinner_html).addClass('disabled').attr('disabled', true);
}
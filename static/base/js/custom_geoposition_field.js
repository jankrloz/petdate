$(document).ready(function () {
    var $div_search = $('.geoposition-search');
    var $input_search = $div_search.find('input[type="search"]');
    var $label_input = $('<label for="id_search"><i class="material-icons">search</i>Busca un lugar o área en el mapa</label>');
    var $address_div = $('.geoposition-address');
    var $address_label = $('<p>Ubicación seleccionada (Arrastra el marcador en el mapa o busca un lugar):</p>');

    $input_search.attr('id', 'id_search').attr('placeholder', null);
    $label_input.removeClass('active');
    $div_search.append($input_search).append($label_input);
    $div_search.addClass('input-field');

    $address_div.prepend($address_label);
    $address_div.insertAfter($div_search);

    $('.geoposition-widget').find('table').hide();
});
var $calendar = $('#calendar');
var $modal_evento = $('#modal_evento');
var eventoIndigo = '#5C6BC0';
var eventoRojo = '#EF5350';
var eventoAzul = '#42A5F5';
var eventoVerde = '#66BB6A';
var eventoRosa = '#EC407A';
var eventoNaranja = '#FFA726';
var eventoMorado = '#AB47BC';

var defaultValidateOptions = {
    name: "required",
    hora_fin_evento: {
        greaterThan: '#hora_evento'
    },
    errorPlacement: function (error, element) {
        $(element).addClass('invalid error').siblings('label').addClass('active').attr('data-error', error.text());
    }
};


$(document).ready(function () {

    jQuery.validator.addMethod("greaterThan", function (value, element, params) {
        return value > $(params).val();
    }, jQuery.validator.format('Debe ser mayor que {0}.'));

    $calendar.fullCalendar({
        events: eventoSourceObj,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: $(window).width() < 480 ? 'agendaDay' : $(window).width() < 960 ? 'agendaWeek' : 'month',
        windowResize: function (view) {
            if ($(window).width() < 480) {
                $('#calendar').fullCalendar('changeView', 'agendaDay');
            } else if ($(window).width() < 960) {
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
            } else {
                $('#calendar').fullCalendar('changeView', 'month');
            }
        },
        lang: 'es',
        editable: true,
        timeFormat: 'H:mm', // uppercase H for 24-hour clock
        displayEventTime: true,
        displayEventEnd: true,
        defaultTimedEventDuration: "01:00:00",
        eventLimit: true, // allow "more" link when too many events
        eventRender: function (event, element) {
            if (event.notas) {
                $(element).addClass('tooltipped').attr({
                    'data-tooltip': 'Nota: ' + event.notas,
                    'data-position': 'top',
                    'data-delay': 1000
                }).tooltip().find('.fc-title').append("<br>" + event.notas);
            }
        },
        eventClick: function (event, element) {
            document.getElementById('form_evento').reset();
            $modal_evento.openModal();
            updateEvent(event);
        },
        eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
            ajaxActualizarEvento(event);
        },
        dayClick: function (date, jsEvent, view) {
            document.getElementById('form_evento').reset();
            $modal_evento.openModal();
            createEvent(date, jsEvent, view);
        }
    });
    updateColorPicker($('#color_evento'), eventoIndigo, false);
});

function createEvent(date, jsEvent, view) {
    var $form = $modal_evento.find('form').first();
    var dateString = moment(date).format("YYYY-MM-DD");
    var timeString = moment(date).format("HH:mm:ss");

    var $input_picker = $form.find('#fecha_evento').pickadate();
    var picker = $input_picker.pickadate('picker');

    $('#titulo_modal').text('Crear evento');

    picker.set('select', dateString, {format: 'yyyy-mm-dd'});

    $form.find('#hora_evento').val("12:00:00").attr('disabled', false);
    $form.find('#hora_fin_evento').val("13:00:00").attr('disabled', false);
    $form.find('#allday_evento').attr('checked', false);
    updateColorPicker($('#color_evento'), eventoIndigo, true);

    Materialize.updateTextFields();

    $('#btn_eliminar_evento').hide();

    $('#btn_guardar_evento').on('click', function () {

        var titulo_evento = $form.find('#titulo_evento').val();
        var fecha_evento = picker.get('select', 'yyyy-mm-dd');
        var hora_evento_inicio = $form.find('#hora_evento').val();
        var hora_evento_fin = $form.find('#hora_fin_evento').val();
        var notas_evento = $form.find('#notas_evento').val();
        var color_evento = $form.find('#color_evento').val();
        var allday_evento = $form.find('#allday_evento').is(':checked');

        $form.validate(defaultValidateOptions);
        if ($form.valid()) {
            var eventObject = {
                title: titulo_evento,
                start: $.fullCalendar.moment(fecha_evento + 'T' + hora_evento_inicio + ':00'),
                end: hora_evento_fin > hora_evento_inicio ? $.fullCalendar.moment(fecha_evento + 'T' + hora_evento_fin + ':00') : $.fullCalendar.moment(fecha_evento + 'T' + hora_evento_inicio + ':00').add(1, 'hours'),
                allDay: allday_evento,
                notas: notas_evento,
                color: color_evento
            };
            $calendar.fullCalendar('renderEvent', eventObject,
                true // make the event "stick"
            );

            ajaxCrearEvento(eventObject);

            document.getElementById('form_evento').reset();
            $(this).off('click');
            $modal_evento.closeModal();
        }
    });
}

function updateEvent(event) {
    var $form = $modal_evento.find('form').first();
    var dateString = moment(event.start).format("YYYY-MM-DD");
    var startTimeString = moment(event.start).format("HH:mm:ss");
    var endTimeString = event.allDay ? null : event.end ? moment(event.end).format("HH:mm:ss") : moment(event.start).add(1, 'hours').format("HH:mm:ss");

    $('#titulo_modal').text('Actualizar evento');
    $form.find('#titulo_evento').val(event.title);

    var $input_picker = $form.find('#fecha_evento').pickadate();
    var picker = $input_picker.pickadate('picker');
    picker.set('select', dateString, {format: 'yyyy-mm-dd'});

    $form.find('#hora_evento').val(startTimeString);
    $form.find('#hora_fin_evento').val(endTimeString);

    $form.find('#allday_evento').attr('checked', event.allDay).trigger('change');
    $form.find('#notas_evento').val(event.notas);
    updateColorPicker($('#color_evento'), event.color, true);

    Materialize.updateTextFields();

    $('#btn_guardar_evento').on('click', function () {
        var titulo_evento = $form.find('#titulo_evento').val();
        var fecha_evento = picker.get('select', 'yyyy-mm-dd');
        var hora_evento_inicio = $form.find('#hora_evento').val();
        var hora_evento_fin = $form.find('#hora_fin_evento').val();
        var notas_evento = $form.find('#notas_evento').val();
        var color_evento = $form.find('#color_evento').val();
        var allday_evento = $form.find('#allday_evento').is(':checked');

        // console.log(hora_evento_inicio, hora_evento_fin, hora_evento_fin > hora_evento_inicio);

        $form.validate(defaultValidateOptions);
        if ($form.valid()) {
            event.title = titulo_evento;
            event.start = $.fullCalendar.moment(fecha_evento + 'T' + hora_evento_inicio + ':00');
            event.end = hora_evento_fin > hora_evento_inicio ? $.fullCalendar.moment(fecha_evento + 'T' + hora_evento_fin + ':00') : $.fullCalendar.moment(event.start).add(1, 'hours');
            event.allDay = allday_evento;
            event.notas = notas_evento;
            event.color = color_evento;

            $calendar.fullCalendar('updateEvent', event);

            ajaxActualizarEvento(event);

            document.getElementById('form_evento').reset();
            $(this).off('click');
            $modal_evento.closeModal();
        }
    });

    $('#btn_eliminar_evento').show().on('click', function () {
        $('#modal_confirmar_eliminar_evento').openModal();
        $('#btn_confirmar_eliminar_evento').on('click', function () {
            if (ajaxEliminarEvento(event.id)) {
                $calendar.fullCalendar('removeEvents', event._id);
            }
            $modal_evento.closeModal();
        });
    });
}

$('input.datepicker').each(function () {
    $(this).pickadate({
        firstDay: 0,
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        close: 'Aceptar'
    });
});

$('input.timepicker').each(function () {
    $(this).pickatime({
        format: 'HH:i',
        formatSubmit: 'HH:i',
        autoclose: true,
        donetext: 'HECHO',
        default: '10:00',
        vibrate: true,
        twelvehour: false
    });
});

function updateColorPicker($input, value, destroy) {
    if (destroy) {
        $input.data('paletteColorPickerPlugin').destroy();
    }
    $input.val(value);
    $input.paletteColorPicker({
        // Color in { key: value } format
        colors: [
            eventoIndigo, eventoAzul, eventoRojo, eventoVerde, eventoNaranja, eventoMorado, eventoRosa
        ]
    });
}

$('#allday_evento').on('change', function () {
    if ($(this).is(':checked')) {
        $('#hora_evento').attr('disabled', true);
        $('#hora_fin_evento').attr('disabled', true);
    }
    else {
        $('#hora_evento').attr('disabled', false);
        $('#hora_fin_evento').attr('disabled', false);
    }
});
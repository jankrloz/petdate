$(document).ready(function () {
    $('.geoposition-widget').find('table').hide();
    $('#btn-guardar-cambios').click(function () {
        $('form').first().submit();
    });

    if (getUrlParameter('success') === 'true') {
        var $toastContent = $('<span><i class="material-icons">check</i> Se ha guardado correctamente</span>');
        Materialize.toast($toastContent, 3000, 'white-toast rounded');
    }
});
$(function () {

    $('textarea#id_descripcion').characterCounter();

    $.validator.setDefaults({
        ignore: []
    });

    $('input, select').each(function () {
        $(this).addClass('validate');
    });
    $('#form-registrar-negocio').validate({
        name: "required",
        nombre: {
            minlength: 6
        },
        descripcion: {
            minlength: 18,
            maxlength: 140
        },
        ubicacion_0: {
            required: true
        },
        ubicacion_1: {
            required: true
        },
        imagen_principal: {
            required: true
        },
        nombre_imagen: {
            required: true
        },
        id_search: {
            required: true
        },
        direccion: {
            required: false
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "imagen_principal") {
                if ($('.jFiler-input').siblings('label.perror').length > 0) {
                    $('.jFiler-input').siblings('label.perror').text('Este campo es obligatorio')
                }
                else
                    $('<label class="perror">' + error.text() + '</label>').insertAfter('.jFiler-input');
            }
            else {
                // $(element).removeClass('valid').addClass('invalid').siblings('label').attr('data-error', error.text());
                $(element).addClass('validate invalid');
                error.addClass('perror').insertAfter($(element).closest('.input-field'));
                Materialize.updateTextFields();
            }
        },
        submitHandler: function (form) {
            var select = $('select[name="categorias"]');
            var geoposicion = $('input[name="ubicacion_0"]');
            if (select.val() != '' && geoposicion != '') {
                addSpinner($('#btn-submit'));
                form.submit();
            }
            if (select.val() == '') {
                window.scrollTo(0, 0);
                select.removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
                if (select.siblings('label.perror').length > 0) {
                    select.siblings('label.perror').text('Este campo es obligatorio')
                }
                else {
                    $('<label class="perror">Este campo es obligatorio</label>').insertAfter(select);
                }
                $('.select-dropdown').removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
            }
            if (geoposicion.val() == '') {
                if (geoposicion.closest('.input-field').siblings('label.perror').length > 0)
                    geoposicion.closest('.input-field').siblings('label.perror').text('Este campo es obligatorio');
                else
                    $('<label class="perror">Este campo es obligatorio</label>').appendTo('.geoposition-search');
            }
        }
    });
});

$(document).ready(function () {
    var filer_default_opts = {
        changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item" style="width:auto">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            itemAppend: '<li class="jFiler-item" style="width:auto">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {},
        uploadFile: {
            url: "./php/upload.php",
            data: {},
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function () {
            },
            success: function (data, el) {
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                });

                console.log(data);
            },
            error: function (el) {
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        }
    };
    $('select#id_categorias').prepend($('<option>', {
        value: "",
        disabled: true,
        selected: true,
        text: 'Selecciona una categoria'
    })).material_select();
    $('#imagen').filer({
        /*changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-folder"></i></div><div class="jFiler-input-text"><h3>Arrastra una imagen</h3> <span style="display:inline-block; margin: 5px 0">o</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Elegir imagen de mi PC</a></div></div>',*/
        showThumbs: true,
        limit: 1,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        dragDrop: filer_default_opts.dragDrop,
        captions: {
            button: "Elegir imagen",
            feedback: "Elige imagen a subir",
            feedback2: "archivos elegidos",
            drop: "Arrastra aquí para subir",
            removeConfirmation: "¿Estas seguro de eliminar este archivo?",
            errors: {
                filesLimit: "Sólo {{fi-limit}} archivos pueden subirse.",
                filesType: "Solo pueden subirse imágenes.",
                filesSize: "{{fi-name}} es muy grande! Por favor elige un archivo de hasta {{fi-fileMaxSize}} Mb.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} Mb.",
                folderUpload: "You are not allowed to upload folders."
            }
        }

    });
});

$('select[name="categorias"]').on('change', function () {
    var select = $('select[name="categorias"]');
    if (select.val() == '') {
        window.scrollTo(0, 0);
        select.removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
        if (select.siblings('label.perror').length > 0) {
            select.siblings('label.perror').text('Este campo es obligatorio')
        }
        else {
            $('<label class="perror">Este campo es obligatorio</label>').insertAfter(select);
        }
        $('.select-dropdown').removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
        e.preventDefault();
    }
    else {
        $('.select-dropdown').removeClass('validate invalid').addClass('valid');
        select.addClass('valid').removeClass('validate invalid');
        select.siblings('label.perror').remove();
    }
});

$('input[name="ubicacion_0"]').on('change', function () {
    var geoposicion = $('input[name="ubicacion_0"]');
    $('.geoposition-search').find('label.perror').remove();
});

$('form').on('submit', function (e) {
    var select = $('select[name="categorias"]');
    var geoposicion = $('input[name="ubicacion_0"]');
    if (select.val() == '') {
        window.scrollTo(0, 0);
        select.removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
        if (select.siblings('label.perror').length > 0) {
            select.siblings('label.perror').text('Este campo es obligatorio')
        }
        else {
            $('<label class="perror">Este campo es obligatorio</label>').insertAfter(select);
        }
        $('.select-dropdown').removeClass('valid').addClass('validate invalid').attr('data-error', "Este campo es obligatorio");
        e.preventDefault();
    }
    else {
        select.addClass('validate valid').removeClass('invalid');
        $('.select-dropdown').addClass('validate valid').removeClass('invalid');
        select.siblings('label.perror').remove();
    }

    if (geoposicion.val() == '') {
        if (geoposicion.closest('.input-field').siblings('label.perror').length > 0)
            geoposicion.closest('.input-field').siblings('label.perror').text('Este campo es obligatorio');
        else
            $('<label class="perror">Este campo es obligatorio</label>').appendTo('.geoposition-search');
        e.preventDefault();
    }
    else {
        $('.geoposition-search').find('label.perror').remove();
    }
    $('#input_direccion').val($('.geoposition-address').text());
});
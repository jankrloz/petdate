var filer_default_opts = {
    changeInput2: '<div class="jFiler-input-dragDrop" style="width: 100%;"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Haz click aqui</h3> <span style="display:inline-block; margin: 0">o</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Busca en tu PC</a></div></div>',
    templates: {
        box: '<div class="row m-b-none jFiler-items-list jFiler-items-grid"></div>',
        item: '<div class="col s12 m6 l4 jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-left">\
                                            <li>\
                                            <input type="radio" name="is_cover" value="{{fi-name}}" id="princ{{fi-name}}">\
                                                <label for="princ{{fi-name}}">Principal</label>\
                                            </li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>',
        itemAppend: '<div class="col s12 m6 l4 jFiler-item">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                           <ul class="list-inline pull-left">\
                                                <li>\
                                                <input type="radio" name="is_cover" value="{{fi-name}}" id="princ{{fi-name}}">\
                                                <label for="princ{{fi-name}}">Principal</label>\
                                                </li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>',
        progressBar: '<div class="bar"></div>',
        itemAppendToEnd: false,
        removeConfirmation: true,
        _selectors: {
            list: '.jFiler-items-list',
            item: '.jFiler-item',
            progressBar: '.bar',
            remove: '.jFiler-item-trash-action'
        }
    },
    captions: {
        button: "Elegir imagen",
        feedback: "Elige imagen a subir",
        feedback2: "archivos elegidos",
        drop: "Arrastra aquí para subir",
        removeConfirmation: "¿Estas seguro de eliminar este archivo?",
        errors: {
            filesType: "Solo pueden subirse imágenes.",
            filesSize: "{{fi-name}} es muy grande! Por favor elige un archivo de hasta {{fi-fileMaxSize}} Mb.",
            filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} Mb.",
            folderUpload: "You are not allowed to upload folders."
        }
    },
    dragDrop: {},
    uploadFile: {
        url: "./php/upload.php",
        data: {},
        type: 'POST',
        enctype: 'multipart/form-data',
        beforeSend: function () {
        },
        success: function (data, el) {
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
            });

            console.log(data);
        },
        error: function (el) {
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
            });
        },
        statusCode: null,
        onProgress: null,
        onComplete: null
    }
};


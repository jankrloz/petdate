$(function () {
    $("#login-form").validate({
        rules: {
            name: "required",
            nombre: {
                minlength: 6,
                maxlength: 120
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            password2: {
                required: true,
                equalTo: "#id_password"
            }
        },
        errorPlacement: function (error, element) {
            $(element).removeClass('valid').addClass('invalid').siblings('label').attr('data-error', error.text());
        },
        submitHandler: function (form) {
            addSpinner($('#btn-submit'));
            form.submit();
        }
    });
});

$(document).on('ready', function () {
    $('#login-form').find('input').each(function () {
        $(this).addClass('validate');
    });
    Materialize.updateTextFields();
});

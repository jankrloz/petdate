from django import forms

from .models import Mascota


class MascotaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')  # globally override the Django >=1.6 default of ':'
        super(MascotaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Mascota
        fields = '__all__'
        widgets = {
            'dueño': forms.HiddenInput(),
            'nombre': forms.TextInput(attrs={
                'required': True,
            }),
            'especie': forms.Select(attrs={
                'required': True,
                'class': 'validate'
            }),
            'raza': forms.TextInput(attrs={
                'required': True,
            }),
            'sexo': forms.Select(attrs={
                'required': True,
                'class': 'validate'
            }),
            'imagen': forms.FileInput(attrs={
                'accept': 'image/*',
                'required': False,
                'class': 'validate'
            }),
            'caracteristicas': forms.Textarea(attrs={
                'class': 'materialize-textarea',
            }),
            'vacunas': forms.Textarea(attrs={
                'class': 'materialize-textarea',
            }),
            "observaciones_medicas": forms.Textarea(attrs={
                'class': 'materialize-textarea',
            }),
            'fecha_nacimiento': forms.DateInput(attrs={
                'class': 'datepicker',
                'required': True,
            })
        }

        labels = {
            'dueño': '',
            'fecha_nacimiento': 'Fecha de nacimiento (Aprox)',
            'caracteristicas': 'Características físicas (color, peso, tamaño, etc.)',
            'esterilizado': 'Esterilizado',
            'castrado': 'Castrado',
            'desparasitado': 'Desparasitado',
            'observaciones_medicas': 'Observaciones Médicas'
        }

from django.db import models
from imagekit.models import ProcessedImageField


def imagen_mascota_upload_to(instance, filename):
    return "mascotas/{nombre}/{file}".format(nombre=instance.dueño.email, file=filename)


class Mascota(models.Model):
    dueño = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=256)
    ESPECIES = ((1, 'Canino'),
                (2, 'Felino'),
                (3, 'Reptil'),
                (4, 'Roedor'),
                (5, 'Anfibio'),
                (6, 'Ave'),
                (7, 'Otro'),
                )
    especie = models.IntegerField(choices=ESPECIES, blank=True, null=True, default=1)
    raza = models.CharField(max_length=256, blank=True)
    imagen = ProcessedImageField(upload_to=imagen_mascota_upload_to,
                                 format='JPEG',
                                 options={'quality': 95})
    SEXOS = ((1, 'Macho'), (2, 'Hembra'))
    sexo = models.PositiveIntegerField(choices=SEXOS)
    fecha_nacimiento = models.DateField(null=True)
    caracteristicas = models.TextField(blank=True)  # color, pelaje, piel, chip, comportamiento,
    desparasitado = models.BooleanField(default=False)
    esterilizado = models.BooleanField(default=False)
    castrado = models.BooleanField(default=False)
    vacunas = models.TextField(blank=True)
    observaciones_medicas = models.TextField(blank=True)

    def __str__(self):
        return self.dueño.email + ': ' + self.nombre

    class Meta:
        verbose_name = 'Mascota'
        verbose_name_plural = 'Mascotas'

    @property
    def get_image_url(self):
        if self.imagen:
            return self.imagen.url
        else:
            from django.contrib.staticfiles.templatetags.staticfiles import static
            return static('base/img/default_pet.png')

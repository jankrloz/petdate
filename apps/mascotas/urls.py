from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^registrar-mascota$', views.registrar_mascota, name='registrar_mascota'),
    url(r'^mis-mascotas$', views.mascotas_usuario, name='mascotas_usuario'),
    url(r'^mis-mascotas/editar/', views.editar_mascota, name='editar_mascota'),
    url(r'^mis-mascotas/eliminar/', views.eliminar_mascota, name='eliminar_mascota'),
]

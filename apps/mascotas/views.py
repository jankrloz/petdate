from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404

from .forms import MascotaForm
from .models import Mascota


@login_required
def mascotas_usuario(request):
    mascotas = Mascota.objects.filter(dueño=request.user)
    return render(request, 'mascotas/mascotas_usuario.html', {'mascotas': mascotas})


@login_required(login_url='/signup')
def registrar_mascota(request):
    mascota_form = MascotaForm(initial={'dueño': request.user.pk}, label_suffix="")
    redirect_to = ''
    if 'next' in request.GET:
        redirect_to = request.GET['next']
    if request.method == 'POST':
        mascota_form = MascotaForm(request.POST, request.FILES, initial={'dueño': request.user.pk})
        if 'redirect_to' in request.POST:
            redirect_to = request.POST['redirect_to']
        if mascota_form.is_valid():
            mascota_form.save()
            if redirect_to != '':
                return HttpResponseRedirect(redirect_to)
            else:
                return redirect(reverse('mascotas_app:mascotas_usuario'))
    return render(request, 'mascotas/registrar_mascota.html', {'form': mascota_form, 'redirect_to': redirect_to})


@login_required
def editar_mascota(request):
    if 'pet' in request.GET:
        try:
            mascota = Mascota.objects.get(id=request.GET.get('pet'))
            mascota_form = MascotaForm(instance=mascota)
            if request.method == 'POST':
                mascota_form = MascotaForm(request.POST, request.FILES, instance=mascota)
                if mascota_form.is_valid():
                    mascota_form.save()
                    return redirect(reverse('mascotas_app:mascotas_usuario'))
            return render(request, 'mascotas/editar_mascota.html', {'form': mascota_form,
                                                                    'id_mascota': mascota.id})

        except Mascota.DoesNotExist as e:
            return redirect(reverse('mascotas_app:mascotas_usuario'))
    else:
        return redirect(reverse('mascotas_app:mascotas_usuario'))


@login_required
def eliminar_mascota(request):
    if request.method == 'POST':
        mascota = get_object_or_404(Mascota, id=request.POST['id_mascota'])
        mascota.delete()
    return redirect(reverse('mascotas_app:mascotas_usuario'))

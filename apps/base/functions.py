import urllib

from django.http import HttpResponseRedirect


def custom_redirect(url, **kwargs):
    params = urllib.parse.urlencode(kwargs)
    return HttpResponseRedirect(url + "?%s" % params)


def check_owner(usuario, negocio):
    if negocio.usuario == usuario:
        return True
    else:
        return False

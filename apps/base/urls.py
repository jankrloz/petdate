from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^FAQ$', views.faq, name='FAQ'),
    url(r'^condiciones-de-uso$', views.condiciones_de_uso, name='condiciones_de_uso')
]
# coding=utf-8
from django.shortcuts import render


def home(request):
    from apps.negocios.models import CategoriasNegocio
    categorias = CategoriasNegocio.objects.all()
    return render(request, 'home.html', {'categorias': categorias})


def faq(request):
    return render(request, "FAQ.html", {})


def condiciones_de_uso(request):
    return render(request, "termCond.html", {})

import logging

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from apps.base.functions import custom_redirect
from apps.usuarios.correos import enviar_correo
from apps.usuarios.models import Usuario
from .forms import LoginForm, SignupForm, UsuarioForm, ContactoSuscripcionForm

logger = logging.getLogger(__name__)


def login_view(request):
    if request.user.is_authenticated():
        return redirect(reverse('base_app:home'))
    login_form = LoginForm()
    redirect_to = ''
    if 'next' in request.GET:
        redirect_to = request.GET['next']
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if 'redirect_to' in request.POST:
            redirect_to = request.POST['redirect_to']
        if login_form.is_valid():
            email = login_form.cleaned_data['email']
            password = login_form.cleaned_data['password']
            user = authenticate(username=email, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if redirect_to != '':
                        return HttpResponseRedirect(redirect_to)
                    else:
                        return redirect(reverse('usuarios_app:perfil_usuario'))
                else:
                    login_form.add_error(None, 'El usuario no está activo')
                    return render(request, 'usuarios/login.html', {'login_form': login_form,
                                                                   'redirect_to': redirect_to})
            else:
                # Return an 'invalid login' error message.
                login_form.add_error(None, 'Los datos del usuario son inválidos')
                return render(request, 'usuarios/login.html', {'login_form': login_form,
                                                               'redirect_to': redirect_to})
        else:
            return render(request, 'usuarios/login.html', {'login_form': login_form,
                                                           'redirect_to': redirect_to})
    return render(request, 'usuarios/login.html', {'login_form': login_form,
                                                   'redirect_to': redirect_to})


def signup_view(request):
    signup_form = SignupForm()
    redirect_to = ''
    if 'next' in request.GET:
        redirect_to = request.GET['next']
    if request.method == 'POST':
        signup_form = SignupForm(request.POST)
        if 'redirect_to' in request.POST:
            redirect_to = request.POST['redirect_to']
        if signup_form.is_valid():
            email = signup_form.cleaned_data['email']
            password = signup_form.cleaned_data['password']
            Usuario.objects.create_user(email=signup_form.cleaned_data['email'],
                                        password=signup_form.cleaned_data['password'],
                                        nombre=signup_form.cleaned_data['nombre']
                                        )
            enviar_correo(destino=email,
                          asunto='Bienvenido a miPetdate',
                          template='bienvenida')
            user = authenticate(username=email, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if redirect_to != '':
                        return HttpResponseRedirect(redirect_to)
                    else:
                        return redirect(reverse('usuarios_app:perfil_usuario'))
            return render(request, 'usuarios/login.html', {'signup_form': signup_form,
                                                           'redirect_to': redirect_to})
        else:
            return render(request, 'usuarios/login.html', {'signup_form': signup_form,
                                                           'redirect_to': redirect_to})

    return render(request, 'usuarios/signup.html', {'signup_form': signup_form,
                                                    'redirect_to': redirect_to})


@login_required
def logout_view(request):
    logout(request)
    return redirect(reverse('usuarios_app:login'))


@login_required
def perfil_usuario(request):
    return render(request, 'usuarios/perfil_usuario.html', {'usuario': request.user})


@login_required
def editar_perfil_usuario(request):
    usuario_form = UsuarioForm(instance=request.user)
    if request.method == 'POST':
        usuario_form = UsuarioForm(request.POST, request.FILES, instance=request.user)
        if usuario_form.is_valid():
            usuario_form.save()
            return custom_redirect(reverse('usuarios_app:perfil_usuario'), success='true')
        else:
            pass
    return render(request, 'usuarios/editar_perfil_usuario.html', {'usuario': request.user,
                                                                   'form': usuario_form})


@login_required
def citas_usuario(request):
    from apps.negocios.models import SolicitudEvento
    citas = SolicitudEvento.objects.filter(mascota__dueño=request.user).order_by('-fecha')
    return render(request, 'usuarios/citas_usuario.html', {'citas': citas})


def beneficios_view(request):
    contacto_form = None
    if request.user.is_authenticated():
        contacto_form = ContactoSuscripcionForm(
            initial={'nombre': request.user.get_full_name, 'correo': request.user.email,
                     'telefono': request.user.telefono})
        if request.method == 'POST':

            contacto_form = ContactoSuscripcionForm(request.POST)
            if contacto_form.is_valid():

                contacto = contacto_form.save(commit=False)
                contacto.usuario = request.user
                contacto.save()
                enviar_correo(destino=contacto_form.cleaned_data['correo'],
                              asunto='Contacto de suscripción',
                              template='contacto_suscripcion')
                return custom_redirect(reverse('negocios_app:negocios_usuario'), success='true')
            else:
                return redirect(reverse('usuarios_app:beneficios'))
    return render(request, 'negocios/beneficios.html', {'contacto_form': contacto_form})

import os
from datetime import date

from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser,
    PermissionsMixin)
from django.db import models
from imagekit.models import ProcessedImageField
from localflavor.mx.models import MXStateField
from pilkit.processors import ResizeToFit

from apps.negocios.models import Negocio


def imagen_usuario_upload_to(instance, filename):
    return "usuarios/{nombre}/{file}".format(nombre=instance.email, file=filename)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, nombre=None, username=None, name=None):

        if not email:
            raise ValueError('Los usuarios deben tener email')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.nombre = nombre
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuario(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=120, null=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    nombre = models.CharField(max_length=120, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    # REQUIRED_FIELDS = ['email']

    # Profile fields
    telefono = models.CharField(max_length=16, blank=True, null=True)
    estado = MXStateField(blank=True, null=True)
    imagen = ProcessedImageField(upload_to=imagen_usuario_upload_to,
                                 format='JPEG',
                                 processors=[ResizeToFit(1920, 1080)],
                                 options={'quality': 80},
                                 null=True)
    periodo_prueba = models.BooleanField(default=False)

    def get_full_name(self):
        if self.nombre:
            return self.nombre
        elif self.username:
            return self.username

    def get_short_name(self):
        if self.nombre:
            split_nombre = self.nombre.split(' ')
            if len(split_nombre) > 3:
                return split_nombre[0] + ' ' + split_nombre[1]
            else:
                return split_nombre[0]
        elif self.username:
            return self.username
        else:
            return self.email.split('@')[0]

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    @property
    def is_staff(self):
        # Simplest possible answer: All admins are staff
        return self.is_admin

    @property
    def tiene_negocios(self):
        return Negocio.objects.filter(usuario=self).exists()

    @property
    def tiene_suscripcion(self):
        try:
            if self.suscripcionusuario and self.suscripcionusuario.estado == '1' and self.suscripcionusuario.fecha_limite >= date.today():
                return True
            else:
                return False
        except SuscripcionUsuario.DoesNotExist:
            return False

    @property
    def tiene_mascotas(self):
        return Negocio.objects.filter(usuario=self).exists()

    @property
    def get_image_filename(self):
        return os.path.basename(self.imagen.name)

    @property
    def get_image_url(self):
        if self.imagen:
            return self.imagen.url
        else:
            from django.contrib.staticfiles.templatetags.staticfiles import static
            return static('base/img/default_avatar.jpg')


class SuscripcionUsuario(models.Model):
    usuario = models.OneToOneField(Usuario)
    fecha_limite = models.DateField()
    prueba = models.BooleanField(default=False)
    STATUS = (('1', 'Activa'), ('2', 'Vencida'), ('3', 'Cancelada'))
    estado = models.CharField(max_length=2, choices=STATUS, default='1')

    def __str__(self):
        return self.usuario.get_short_name() + ' (' + str(self.get_estado_display()) + ') - Límite: ' + str(
            self.fecha_limite)

    class Meta:
        verbose_name = 'Suscripción'
        verbose_name_plural = 'Suscripciones'


class ContactoSuscripcion(models.Model):
    usuario = models.ForeignKey(Usuario)
    nombre = models.CharField(max_length=120)
    correo = models.EmailField()
    telefono = models.CharField(max_length=16)

    def __str__(self):
        return self.usuario.nombre

    class Meta:
        verbose_name = 'Contacto de Suscripción'
        verbose_name_plural = 'Contactos de Suscripción'

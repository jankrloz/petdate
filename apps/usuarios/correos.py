import logging

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

logger = logging.getLogger(__name__)


def enviar_correo(destino, asunto, template):
    try:
        logger.error("correo")
        subject = asunto
        from_email = "miPetdate <%s>" % settings.EMAIL_HOST_USER
        to = [destino, ]

        html_content = render_to_string("correos/%s.html" % template)
        text_content = strip_tags(html_content)

        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    except Exception as e:
        logger.error("correos", e)


def mailing_1_bienvenida(destino=None, asunto='¡Bienvenido a miPetdate!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = [destino, ]
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/bienvenida.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_2_solicitud_cita(destino=['contactopetdate@gmail.com', ], asunto='¡Solicitaste una cita en mipetdate.com!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/solicitud_cita.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_3_respuesta_cita(destino=['contactopetdate@gmail.com', ], asunto='¡Sobre tu cita en mipetdate.com!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/respuesta_cita.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


'''
     Mail para correo masivo
'''


def mailing_4_suscripcion(destino=['contactopetdate@gmail.com', ], asunto='¡Mi Petadate en México!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/PetDateMexico.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_5_bienvenida_vip(destino=['contactopetdate@gmail.com', ], asunto='miPetdate: Contacto para Suscripción'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/contacto_suscripcion.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_6_suscripcion(destino=['contactopetdate@gmail.com', ], asunto='¡Suscripción mipetdate.com autorizada!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/suscripcion.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_7_solicitaron_cita(destino=['contactopetdate@gmail.com', ],
                               asunto='¡Solicitaron una cita en mipetdate.com!'):
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/solicitud_cita_negocio.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass


def mailing_8_renovacion_suscripcion(destino=['contactopetdate@gmail.com', ],
                                     asunto='¡Renovación de suscripción mipetdate.com!'):
    '''
        preguntar a juancarlos si tiene algun cronos o algo para las tareas diarias
    '''
    subject = asunto
    from_email = "miPetdate <contactopetdate@gmail.com>"
    to = destino
    url_accion = ""
    data = {
        'asunto': asunto,
        'url_accion': url_accion,
        'correo': destino[0]
    }

    html_content = render_to_string("correos/renovacion_suscripcion.html", context=data)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        pass

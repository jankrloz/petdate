from django.contrib import admin

from .models import Usuario, SuscripcionUsuario, ContactoSuscripcion

admin.site.register(Usuario)
admin.site.register(SuscripcionUsuario)
admin.site.register(ContactoSuscripcion)

from django import forms
from localflavor.mx.mx_states import STATE_CHOICES

from .models import Usuario, ContactoSuscripcion


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['email', 'nombre', 'telefono', 'estado', 'imagen']
        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'validate',
                'required': True
            }),
            'nombre': forms.TextInput(attrs={
                'class': 'validate',
                'required': True
            }),
            'telefono': forms.TextInput(attrs={
                'class': 'validate',
                'required': True,
            }),
            'estado': forms.Select(choices=STATE_CHOICES, attrs={
                'class': 'validate',
                'required': True
            }),
            'imagen': forms.FileInput(attrs={
                'class': 'validate',
                'accept': 'image/*',
                'required': False,
            }),
        }
        labels = {
            'email': 'Correo electrónico'
        }


class LoginForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [
            "email",
            "password"
        ]
        labels = {
            'email': 'Correo',
            'password': 'Contraseña'
        }
        widgets = {
            'email': forms.EmailInput(attrs={
                'required': True,
                'class': 'validate'
            }),
            'password': forms.PasswordInput(attrs={
                'required': True,
                'min-length': 6,
                'class': 'validate'
            })
        }

    def clean(self):
        email = self.cleaned_data['email']
        cleaned_data = self.cleaned_data
        # ... do some cross-fields validation for the subclass here
        '''
        try:
            Usuario.objects.get(email=email)
        except Usuario.DoesNotExist:
            raise forms.ValidationError('El usuario no existe')
        '''

        # Then call the clean() method of the super  class
        # super(LoginForm, self).clean()

        # Finally, return the cleaned_data
        return cleaned_data


class SignupForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [
            "nombre",
            "email",
            "password"
        ]
        labels = {
            'nombre': 'Nombre completo',
            'email': 'Correo',
            'password': 'Contraseña'
        }
        widgets = {
            'nombre': forms.TextInput(attrs={
                'required': True,
                'min-length': 6,
                'class': 'validate'
            }),
            'email': forms.EmailInput(attrs={
                'required': True,
                'class': 'validate'
            }),
            'password': forms.PasswordInput(attrs={
                'required': True,
                'min-length': 6,
                'class': 'validate'
            })
        }

    password2 = forms.CharField(max_length=30, widget=forms.PasswordInput(), label='Repite la contraseña')

    def clean_username(self):  # check if username dos not exist before
        try:
            Usuario.objects.get(email=self.cleaned_data['email'])  # get user from user model
        except Usuario.DoesNotExist:
            return self.cleaned_data['email']

        raise forms.ValidationError("El correo ya esta registrado")

    def clean(self):  # check if password 1 and password2 match each other
        self.clean_username()
        if 'password' in self.cleaned_data and 'password2' in self.cleaned_data:  # check if both pass first validation
            if self.cleaned_data['password'] != self.cleaned_data['password2']:  # check if they match each other
                raise forms.ValidationError("Las contraseñas no coinciden")

        return self.cleaned_data


class ContactoSuscripcionForm(forms.ModelForm):
    class Meta:
        model = ContactoSuscripcion
        fields = ['nombre', 'correo', 'telefono']

        widgets = {
            'nombre': forms.TextInput(attrs={
                'class': 'white-input',
                'required': True,
                'placeholder': 'Nombre de contacto'
            }),
            'correo': forms.TextInput(attrs={
                'class': 'white-input',
                'required': True,
                'type': 'email',
                'placeholder': 'Correo de contacto'
            }),
            'telefono': forms.TextInput(attrs={
                'class': 'white-input',
                'required': True,
                'placeholder': 'Teléfono de contacto'
            }),
        }

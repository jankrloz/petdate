from io import BytesIO
from urllib.request import urlopen

from django.core.files.base import ContentFile
from social.backends.google import GooglePlusAuth


def get_avatar(backend, strategy, details, response, is_new=False, user=None, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = 'https://graph.facebook.com/%s/picture?type=large' % response['id']
        if url and is_new:
            input_file = BytesIO(urlopen(url).read())  # img_url is simply the image URL
            user.imagen.save("profile.jpg", ContentFile(input_file.getvalue()), save=False)
            user.save()
    if isinstance(backend, GooglePlusAuth):
        if response.get('image') and response['image'].get('url'):
            url = response['image'].get('url')
            ext = url.split('.')[-1]
            if url and is_new:
                input_file = BytesIO(urlopen(url).read())  # img_url is simply the image URL
                user.imagen.save(
                    '{0}.{1}'.format('avatar', ext),
                    ContentFile(input_file.getvalue()),
                    save=False
                )
                user.save()


def get_name(backend, strategy, details, response, is_new=False, user=None, *args, **kwargs):
    if backend.name == 'facebook':
        if is_new:
            user.nombre = response.get('name')
            user.save()

    if isinstance(backend, GooglePlusAuth):
        if is_new:
            user.nombre = response.get('displayName')
            user.save()


def user_details(strategy, details, is_new=False, user=None, *args, **kwargs):
    """Update user details using data from provider."""
    if user and is_new:
        changed = False  # flag to track changes
        protected = ('username', 'id', 'pk', 'email') + \
                    tuple(strategy.setting('PROTECTED_USER_FIELDS', []))

        # Update user model attributes with the new data sent by the current
        # provider. Update on some attributes is disabled by default, for
        # example username and id fields. It's also possible to disable update
        # on fields defined in SOCIAL_AUTH_PROTECTED_FIELDS.
        for name, value in details.items():
            if value and hasattr(user, name):
                # Check https://github.com/omab/python-social-auth/issues/671
                current_value = getattr(user, name, None)
                if not current_value or name not in protected:
                    changed |= current_value != value
                    setattr(user, name, value)

        if changed:
            strategy.storage.user.changed(user)

# coding=utf-8
from datetime import date


class CheckSuscriptionMiddleware(object):
    def process_request(self, request):
        if request.user.is_authenticated():
            from apps.usuarios.models import Usuario, SuscripcionUsuario
            try:
                usuario = Usuario.objects.get(id=request.user.id)
                if usuario.suscripcionusuario and usuario.suscripcionusuario.estado == '1' and usuario.suscripcionusuario.fecha_limite < date.today():
                    usuario.suscripcionusuario.estado = '2'
                    usuario.suscripcionusuario.save()
            except SuscripcionUsuario.DoesNotExist:
                pass
        return None

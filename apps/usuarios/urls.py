from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login', views.login_view, name='login'),
    url(r'^signup', views.signup_view, name='signup'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^perfil/$', views.perfil_usuario, name='perfil_usuario'),
    url(r'^perfil/editar$', views.editar_perfil_usuario, name='editar_perfil_usuario'),
    url(r'^mis-citas$', views.citas_usuario, name='citas_usuario'),
    url(r'^suscripcion/beneficios/$', views.beneficios_view, name='beneficios'),
]
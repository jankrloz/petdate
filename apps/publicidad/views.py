from django.core.urlresolvers import reverse
from django.shortcuts import render

from apps.base.functions import custom_redirect
from .forms import ContactoPublicidadForm


def contacto_publicidad_view(request):
    contacto_publicidad_form = ContactoPublicidadForm()
    if request.method == 'POST':
        contacto_publicidad_form = ContactoPublicidadForm(request.POST)
        if contacto_publicidad_form.is_valid():
            contacto_publicidad_form.save()
            return custom_redirect(reverse('publicidad_app:contacto_publicidad'), success='true')

    return render(request, 'publicidad/contacto_publicidad.html', {'form': contacto_publicidad_form})

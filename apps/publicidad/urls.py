# coding=utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^publicidad/contacto$', views.contacto_publicidad_view, name='contacto_publicidad'),
]

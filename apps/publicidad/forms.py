# coding=utf-8
from django import forms

from .models import ContactoPublicidad


class ContactoPublicidadForm(forms.ModelForm):
    class Meta:
        model = ContactoPublicidad
        fields = '__all__'

# coding=utf-8
from django.db import models


class ContactoPublicidad(models.Model):
    nombre_empresa = models.CharField(max_length=120)
    nombre_interesado = models.CharField(max_length=120)
    correo_contacto = models.EmailField()
    telefono = models.CharField(max_length=18)

    def __str__(self):
        return self.nombre_empresa

    class Meta:
        verbose_name = 'Contacto publicidad'
        verbose_name_plural = 'Contactos publicidad'

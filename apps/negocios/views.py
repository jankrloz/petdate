# coding=utf-8
import json
import logging
from datetime import datetime, timedelta, date

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms import modelformset_factory
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import dateparse

from apps.base.functions import custom_redirect, check_owner
from apps.mascotas.models import Mascota
from apps.usuarios.correos import enviar_correo
from apps.usuarios.models import Usuario, SuscripcionUsuario
from .forms import NegocioForm, ContactoNegocioForm, HorarioNegocioForm, BaseHorarioFormSet, ServiciosNegocioForm, \
    BaseServiciosFormSet, ComentariosNegocioForm
from .models import Negocio, ContactoNegocio, HorarioSemanalNegocio, ServiciosNegocio, ImagenNegocio, Evento, \
    SolicitudEvento, CategoriasNegocio

logger = logging.getLogger(__name__)


def negocios_view(request):
    negocios = Negocio.objects.all()
    return render(request, 'negocios/negocios.html', {'negocios': negocios})


def buscar_negocios__old(request):
    categorias = CategoriasNegocio.objects.all().order_by('nombre_publico')

    lista_negocios = Negocio.objects.all()

    if 'cat' in request.GET:
        lista_negocios = lista_negocios.filter(categorias__id=request.GET['cat'])

    if request.method == 'POST':

        if request.POST['latitud'] and request.POST['longitud']:
            import math

            lat = float(request.POST['latitud'])
            long = float(request.POST['longitud'])

            # Haversine formula = https://en.wikipedia.org/wiki/Haversine_formula
            R = 6378.1  # earth radius
            bearing = 1.57  # 90 degrees bearing converted to radians.
            distance = 5  # distance in km

            lat1 = math.radians(lat)  # lat in radians
            long1 = math.radians(long)  # long in radians

            lat2 = math.asin(math.sin(lat1) * math.cos(distance / R) +
                             math.cos(lat1) * math.sin(distance / R) * math.cos(bearing))

            long2 = long1 + math.atan2(math.sin(bearing) * math.sin(distance / R) * math.cos(lat1),
                                       math.cos(distance / R) - math.sin(lat1) * math.sin(lat2))

            lat2 = math.degrees(lat2)
            long2 = math.degrees(long2)

            negocios = lista_negocios.exclude(ubicacion__isnull=True).exclude(ubicacion__exact='')

            lista_negocios = []

            for negocio in negocios:
                if negocio.ubicacion:
                    latitud = negocio.ubicacion.latitude
                    longitud = negocio.ubicacion.longitude

                    if lat1 <= latitud <= lat2 or long1 <= longitud <= long2:
                        lista_negocios.append(negocio)

            return render(request, 'negocios/buscar_negocios.html', {'negocios': lista_negocios,
                                                                     'categorias': categorias,
                                                                     'latitud': lat,
                                                                     'longitud': long})

    return render(request, 'negocios/buscar_negocios.html', {'negocios': lista_negocios,
                                                             'categorias': categorias,})


def buscar_negocios(request):
    categorias = CategoriasNegocio.objects.all().order_by('nombre_publico')

    negocios = Negocio.objects.all()

    if request.method == 'POST':

        query = request.POST["query"]
        categoria = None

        try:
            if 'categoria' in request.POST and request.POST['categoria'] != '':
                negocios = negocios.filter(categorias__id=request.POST['categoria'])
                categoria = CategoriasNegocio.objects.get(id=request.POST['categoria'])

            if query != "":
                negocios = negocios.filter(
                        Q(nombre__icontains=query) |
                        Q(descripcion__icontains=query)
                ).distinct()

            if request.POST['latitud'] != '' and request.POST['longitud'] != '':
                import math

                lat = math.radians(float(request.POST['latitud']))
                long = math.radians(float(request.POST['longitud']))
                R = 6378.1  # earth radius
                distance = 5  # distance in km
                # negocios = negocios.exclude(ubicacion__isnull=True).exclude(ubicacion__exact='')

                lista_negocios = []
                for negocio in negocios:
                    if negocio.ubicacion:
                        latitud = math.radians(float(negocio.ubicacion.latitude))
                        longitud = math.radians(float(negocio.ubicacion.longitude))
                        distancia = R * (
                            math.acos(
                                    (
                                        math.cos(
                                                lat
                                        ) * math.cos(
                                                latitud
                                        ) * math.cos(
                                                long - longitud
                                        )
                                    ) + (
                                        math.sin(
                                                lat
                                        ) * math.sin(
                                                latitud
                                        )
                                    )
                            )
                        )
                        if distancia <= distance:
                            lista_negocios.append(negocio)
                if len(lista_negocios) > 0:
                    lista_negocios = sorted(lista_negocios, key=lambda t: t.get_rating)
                    # print("filtrados")
            else:
                lista_negocios = negocios
            return render(request, 'negocios/buscar_negocios.html', {'negocios': lista_negocios,
                                                                     'categorias': categorias,
                                                                     'ubicacion': request.POST['ubicacion'],
                                                                     'categoria': categoria,
                                                                     'query': query})
        except Exception as e:
            pass  # print(e.args)
    # print("todos")
    return render(request, 'negocios/buscar_negocios.html', {'negocios': negocios,
                                                             'categorias': categorias,})


def negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    comentario_form = ComentariosNegocioForm()
    comentarios_disponibles = True
    comentario_usuario = None
    if request.user.is_anonymous():
        comentarios_disponibles = False
    elif negocio.comentariosnegocio_set.filter(usuario=Usuario.objects.get(id=request.user.id)).exists():
        comentarios_disponibles = False
        comentario_usuario = negocio.comentariosnegocio_set.filter(usuario=request.user).first()

    if request.method == 'POST':
        if comentarios_disponibles:
            comentario_form = ComentariosNegocioForm(request.POST)
            if comentario_form.is_valid():
                comentario = comentario_form.save(commit=False)
                comentario.usuario = request.user
                comentario.negocio = negocio
                comentario.save()
                return redirect(reverse('negocios_app:negocio_view', kwargs={'slug': slug}))

    return render(request, 'negocios/negocio.html', {'negocio': negocio,
                                                     'es_dueno': check_owner(request.user, negocio),
                                                     'comentario_form': comentario_form,
                                                     'comentarios_disponibles': comentarios_disponibles,
                                                     'comentario_usuario': comentario_usuario or None,})


@login_required
def negocios_usuario_view(request):
    negocios = Negocio.objects.filter(usuario=request.user)
    return render(request, 'negocios/negocios_usuario.html', {'negocios': negocios})


@login_required(login_url='/signup')
def registrar_negocio_view(request):
    """
    if Negocio.objects.filter(usuario=request.user).exists():
        return redirect(reverse('base_app:home'))
    """
    negocio_form = NegocioForm()
    if request.method == 'POST':
        negocio_form = NegocioForm(request.POST, request.FILES)
        if negocio_form.is_valid():
            negocio = negocio_form.save(commit=False)
            negocio.usuario = Usuario.objects.get(pk=request.user.pk)
            negocio.direccion = request.POST['direccion']
            negocio = negocio_form.save()
            enviar_correo(destino=negocio.usuario.email,
                          asunto='miPetdate: Contacto para Suscripción',
                          template='contacto_suscripcion')
            ImagenNegocio.objects.create(negocio=negocio, imagen=request.FILES['imagen_principal'], is_cover_image=True)

            if not negocio.usuario.periodo_prueba:
                # Creando su periodo de prueba
                from django.conf import settings
                try:
                    if not negocio.usuario.suscripcionusuario:
                        SuscripcionUsuario.objects.create(usuario=negocio.usuario, prueba=True,
                                                          estado='1',
                                                          fecha_limite=date.today() + timedelta(
                                                                  days=settings.DIAS_PRUEBA))
                        negocio.periodo_prueba = True
                        negocio.save()
                except SuscripcionUsuario.DoesNotExist as e:
                    SuscripcionUsuario.objects.create(usuario=negocio.usuario, prueba=True,
                                                      estado='1',
                                                      fecha_limite=date.today() + timedelta(
                                                              days=settings.DIAS_PRUEBA))
                    negocio.periodo_prueba = True
                    negocio.save()

            return custom_redirect(reverse('negocios_app:configurar_servicios', kwargs={'slug': negocio.slug}),
                                   new='true')
    return render(request, 'negocios/registrar_negocio.html', {'negocio_form': negocio_form})


def configurar_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    negocio_form = NegocioForm(instance=negocio)
    if request.method == 'POST':
        negocio_form = NegocioForm(request.POST, instance=negocio)
        if negocio_form.is_valid():
            negocio = negocio_form.save(commit=False)
            negocio.direccion = request.POST['direccion']
            negocio.save()
            return custom_redirect(reverse('negocios_app:configurar_negocio', kwargs={'slug': negocio.slug}),
                                   success='true')
        else:
            negocio_form.add_error(None, 'El formulario es inválido')

    return render(request, 'negocios/configuraciones_negocio/configurar_basico_negocio.html',
                  {'negocio': negocio,
                   'negocio_form': negocio_form})


def configurar_contacto_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    try:
        contacto_negocio, created = ContactoNegocio.objects.get_or_create(negocio=negocio)
    except Exception as e:
        contacto_negocio = None

    contacto_negocio_form = ContactoNegocioForm(instance=contacto_negocio)
    if request.method == 'POST':
        contacto_negocio_form = ContactoNegocioForm(request.POST, instance=contacto_negocio)
        if contacto_negocio_form.is_valid():
            contacto_negocio_form.save()
            return custom_redirect(reverse('negocios_app:configurar_contacto', kwargs={'slug': negocio.slug}),
                                   success='true')
        else:
            contacto_negocio.add_error(None, 'El formulario es inválido')

    return render(request, 'negocios/configuraciones_negocio/configurar_contacto_negocio.html',
                  {'negocio': negocio,
                   'contacto_negocio_form': contacto_negocio_form})


def configurar_horario_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    # if request.GET.get('success') == 'true':
    #    return redirect(reverse('negocios_app:configurar_horario', kwargs={'slug': negocio.slug}))

    HorariosNegocioFormset = modelformset_factory(HorarioSemanalNegocio, form=HorarioNegocioForm, min_num=1,
                                                  can_delete=True,
                                                  extra=0, formset=BaseHorarioFormSet)
    horario_formset = HorariosNegocioFormset(negocio=negocio)
    if request.method == 'POST':
        horario_formset = HorariosNegocioFormset(request.POST, negocio=negocio)
        if horario_formset.is_valid():
            instances = horario_formset.save(commit=False)
            for instance in instances:
                instance.negocio = negocio
                instance.save()
            for deleted in horario_formset.deleted_objects:
                deleted.delete()

            return custom_redirect(reverse('negocios_app:configurar_horario', kwargs={'slug': negocio.slug}),
                                   success='true')
        else:
            pass

    return render(request, 'negocios/configuraciones_negocio/configurar_horario_negocio.html',
                  {'negocio': negocio,
                   'formset': horario_formset
                   })


def configurar_servicios_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    ServiciosNegocioFormset = modelformset_factory(ServiciosNegocio, form=ServiciosNegocioForm, min_num=1,
                                                   can_delete=True,
                                                   extra=0, formset=BaseServiciosFormSet)
    servicios_formset = ServiciosNegocioFormset(negocio=negocio)
    if request.method == 'POST':
        servicios_formset = ServiciosNegocioFormset(request.POST, negocio=negocio)
        if servicios_formset.is_valid():
            instances = servicios_formset.save(commit=False)
            for instance in instances:
                instance.negocio = negocio
                instance.save()
            for deleted in servicios_formset.deleted_objects:
                deleted.delete()

            return custom_redirect(reverse('negocios_app:configurar_servicios', kwargs={'slug': negocio.slug}),
                                   success='true')

    return render(request, 'negocios/configuraciones_negocio/configurar_servicios_negocio.html',
                  {'negocio': negocio,
                   'formset': servicios_formset
                   })


def configurar_galeria_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()
    imagenes = negocio.imagennegocio_set.all()
    if request.method == 'POST':

        for imagen in request.FILES.getlist('imagen[]'):
            ImagenNegocio.objects.create(negocio=negocio, imagen=imagen)
        for imagen in ImagenNegocio.objects.filter(negocio=negocio):
            nombre = imagen.get_image_filename
            if 'jfiler-items-exclude-imagen-0' in request.POST and \
                            nombre in request.POST['jfiler-items-exclude-imagen-0']:
                imagen.delete()
            else:
                imagen.is_cover_image = False
                if nombre == request.POST['is_cover']:
                    imagen.is_cover_image = True
                imagen.save()
        return custom_redirect(reverse('negocios_app:configurar_galeria', kwargs={'slug': negocio.slug}),
                               success='true')

    return render(request, 'negocios/configuraciones_negocio/configurar_galeria_negocio.html',
                  {'negocio': negocio, 'imagenes': imagenes,
                   'cover': negocio.get_cover_image})


def dashboard_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    if not check_owner(request.user, negocio):
        raise Http404('Lo sentimos, no tienes permisos para ver esta página')
    return render(request, 'negocios/dashboard_negocio.html', {'negocio': negocio})


def calendario_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    if not check_owner(request.user, negocio):
        raise Http404('Lo sentimos, no tienes permisos para ver esta página')
    if not negocio.tiene_suscripcion:
        raise Http404()
    return render(request, 'negocios/calendario_negocio.html', {'negocio': negocio})


# API eventos

def ajax_obtener_eventos(request):
    negocio = get_object_or_404(Negocio, id=request.GET.get('id_negocio'))
    if not check_owner(request.user, negocio):
        raise Http404('Lo sentimos, no tienes permisos para ver esta página')
    if request.is_ajax():
        eventos = Evento.objects.filter(
                negocio=negocio,
                fecha_inicio__range=[request.GET.get('start'), request.GET.get('end')]
        )
        json_list = []
        for evento in eventos:
            id = evento.id
            title = evento.titulo
            start = evento.fecha_inicio
            end = evento.fecha_fin or None
            allDay = evento.all_day
            notas = evento.notas
            color = evento.color

            json_entry = {'id': id, 'title': title, 'start': start, 'end': end,
                          'allDay': allDay, 'notas': notas, 'color': color}
            json_list.append(json_entry)
        return JsonResponse(json_list, safe=False)


def ajax_crear_evento(request):
    json_data = json.loads(request.body.decode("utf-8"))
    negocio = get_object_or_404(Negocio, id=json_data('id_negocio'))
    if not check_owner(request.user, negocio):
        raise Http404('Lo sentimos, no tienes permisos para ver esta página')
    if request.is_ajax():
        try:
            Evento.objects.create(
                    negocio=Negocio.objects.get(id=json_data['id_negocio']),
                    titulo=json_data['event']['title'],
                    fecha_inicio=dateparse.parse_datetime(json_data['event']['start']),
                    fecha_fin=dateparse.parse_datetime(json_data['event']['end']),
                    all_day=True if json_data['event']['allDay'] is 'true' else False,
                    notas=json_data['event']['notas'],
                    color=json_data['event']['color'],
            )
            return HttpResponse(status=200)
        except Exception as e:
            return HttpResponse(e, status=500)


def ajax_eliminar_evento(request):
    if request.is_ajax():
        Evento.objects.get(id=request.POST['id_evento']).delete()
        return HttpResponse('ok', status=200)


def ajax_actualizar_evento(request):
    if request.is_ajax():
        json_data = json.loads(request.body.decode("utf-8"))
        evento = Evento.objects.get(id=json_data['event']['id'])
        evento.titulo = json_data['event']['title']
        evento.fecha_inicio = datetime.strptime(json_data['event']['start'], "%Y-%m-%dT%H:%M:%S")
        evento.fecha_fin = datetime.strptime(json_data['event']['end'], "%Y-%m-%dT%H:%M:%S")
        evento.all_day = True if json_data['event']['allDay'] is 'true' else False
        evento.notas = json_data['event']['notas']
        evento.color = json_data['event']['color']
        evento.save()
        return HttpResponse(status=200)


@login_required
def ajax_solicitar_evento(request):
    if request.is_ajax():
        try:
            negocio = Negocio.objects.get(id=request.POST['id_negocio'])
            if not negocio.tiene_suscripcion:
                raise Http404()
            SolicitudEvento.objects.create(
                    negocio=negocio,
                    mascota=Mascota.objects.get(id=request.POST['id_mascota']) or None,
                    servicio=ServiciosNegocio.objects.get(id=request.POST['id_servicio']) or None,
                    fecha=request.POST['fecha_cita'] + ' ' + request.POST['hora_cita']
            )
            enviar_correo(destino=request.user.email,
                          asunto='¡Solicitaste una cita en mipetdate.com!',
                          template='solicitud_cita')
            enviar_correo(destino=negocio.usuario.email,
                          asunto='¡Solicitaron una cita en mipetdate.com!',
                          template='solicitud_cita_negocio')
            return HttpResponse(status=200)
        except Exception as e:
            logger.exception('Error al solicitar cita', e.args)
            return HttpResponse(status=500)


def citas_negocio_view(request, slug):
    try:
        negocio = get_object_or_404(Negocio, slug=slug)
    except Negocio.MultipleObjectsReturned:
        negocio = Negocio.objects.filter(slug=slug).first()

    if not check_owner(request.user, negocio):
        raise Http404('Lo sentimos, no tienes permisos para ver esta página')
    if not negocio.tiene_suscripcion:
        raise Http404()
    citas = SolicitudEvento.objects.filter(negocio=negocio).order_by('-fecha')
    return render(request, 'negocios/citas_negocio.html', {'negocio': negocio, 'citas': citas})


def ajax_activar_cita(request):
    if request.is_ajax():
        try:
            cita = SolicitudEvento.objects.get(id=request.POST['id_cita'])
            cita.estado = '2'
            cita.evento = Evento.objects.create(
                    negocio=Negocio.objects.get(id=request.POST['id_negocio']),
                    titulo='Cita',
                    fecha_inicio=cita.fecha,
                    fecha_fin=cita.fecha + timedelta(hours=1),
                    color='#EF5350'
            )
            cita.save()
            enviar_correo(destino=cita.mascota.dueño.email,
                          asunto='¡Sobre tu cita en mipetdate.com!',
                          template='respuesta_cita')
            return HttpResponse('ok', status=200)
        except Exception as e:
            logger.error('Error al activar cita', e)
            return HttpResponse('error', status=500)


def ajax_cancelar_cita(request):
    if request.is_ajax():
        try:
            cita = SolicitudEvento.objects.get(id=request.POST['id_cita'])
            cita.estado = '3'  # Rechazado
            cita.save()
            if cita.evento:
                cita.evento.delete()
            enviar_correo(destino=cita.mascota.dueño.email,
                          asunto='¡Sobre tu cita en mipetdate.com!',
                          template='respuesta_cita')
            return HttpResponse('ok', status=200)
        except SolicitudEvento.DoesNotExist as e:
            logger.error('Error al cancelar cita', e.args)
            return HttpResponse('error', status=500)

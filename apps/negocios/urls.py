from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^negocios/$', views.negocios_view, name='negocios'),
    url(r'^negocios/registrar/$', views.registrar_negocio_view, name='registrar_negocio'),

    url(r'^negocios/buscar/$', views.buscar_negocios, name='buscar_negocios'),

    url(r'^negocios/(?P<slug>[\w-]+)/$', views.negocio_view,
        name='negocio_view'),

    url(r'^negocios/(?P<slug>[\w-]+)/dashboard/$', views.dashboard_negocio_view,
        name='dashboard'),

    url(r'^negocios/(?P<slug>[\w-]+)/citas/$', views.citas_negocio_view,
        name='citas'),

    # Calendario y citas
    url(r'^negocios/(?P<slug>[\w-]+)/calendario/$', views.calendario_negocio_view,
        name='calendario'),

    url(r'^citas/api/activar', views.ajax_activar_cita, name='ajax_activar_cita'),
    url(r'^citas/api/rechazar', views.ajax_cancelar_cita, name='ajax_cancelar_cita'),
    url(r'^eventos/api/get', views.ajax_obtener_eventos, name='ajax_obtener_eventos'),
    url(r'^eventos/api/solicitar', views.ajax_solicitar_evento, name='ajax_solicitar_evento'),
    url(r'^eventos/api/create$', views.ajax_crear_evento, name='ajax_crear_evento'),
    url(r'^eventos/api/delete$', views.ajax_eliminar_evento, name='ajax_eliminar_evento'),
    url(r'^eventos/api/update$', views.ajax_actualizar_evento, name='ajax_actualizar_evento'),

    # Configuraciones
    url(r'^negocios/(?P<slug>[\w-]+)/configurar/$', views.configurar_negocio_view,
        name='configurar_negocio'),
    url(r'^negocios/(?P<slug>[\w-]+)/configurar/contacto/$', views.configurar_contacto_negocio_view,
        name='configurar_contacto'),
    url(r'^negocios/(?P<slug>[\w-]+)/configurar/horario/$', views.configurar_horario_negocio_view,
        name='configurar_horario'),
    url(r'^negocios/(?P<slug>[\w-]+)/configurar/servicios/$', views.configurar_servicios_negocio_view,
        name='configurar_servicios'),
    url(r'^negocios/(?P<slug>[\w-]+)/configurar/galeria/$', views.configurar_galeria_negocio_view,
        name='configurar_galeria'),
    url(r'^mis-negocios/$', views.negocios_usuario_view, name='negocios_usuario'),
]

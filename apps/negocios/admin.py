from django.contrib import admin

from .models import *

admin.site.register(CategoriasNegocio)
admin.site.register(ImagenNegocio)


class GaleriaNegocioInline(admin.StackedInline):
    model = ImagenNegocio
    extra = 0


class ContactoNegocioInline(admin.StackedInline):
    model = ContactoNegocio


class ComentariosNegocioInline(admin.StackedInline):
    model = ComentariosNegocio
    extra = 0


class ServiciosNegocioInline(admin.StackedInline):
    model = ServiciosNegocio
    extra = 0


class HorarioNegocioInline(admin.StackedInline):
    model = HorarioSemanalNegocio
    extra = 0


class EventosNegocioInline(admin.StackedInline):
    model = Evento
    extra = 0


class SolicitudCitasNegocioInline(admin.StackedInline):
    model = SolicitudEvento
    extra = 0


@admin.register(Negocio)
class NegocioAdmin(admin.ModelAdmin):
    inlines = [
        GaleriaNegocioInline,
        ServiciosNegocioInline,
        ContactoNegocioInline,
        ComentariosNegocioInline,
        HorarioNegocioInline,
        EventosNegocioInline,
        SolicitudCitasNegocioInline,
    ]

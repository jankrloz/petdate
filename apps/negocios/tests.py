import names
import numpy as np

from apps.usuarios.models import Usuario
from .models import Negocio, CategoriasNegocio


def crear_negocio():
    nombre = names.get_first_name()
    email = '%s@gmail.com' % nombre
    usuario = Usuario.objects.create_user(email=email,
                                          nombre=nombre)

    import random
    random_idx = random.randint(0, CategoriasNegocio.objects.count() - 1)
    categoria = CategoriasNegocio.objects.all()[random_idx]

    negocio = Negocio.objects.create(usuario=usuario, nombre='Negocio de %s' % nombre)
    x, y = create_random_point(19.4912499, -99.1994474, 1000)
    negocio.ubicacion = '%s,%s' % (x, y)
    negocio.save()
    return negocio


def create_random_point(x0, y0, distance):
    """
            Utility method for simulation of the points
    """
    r = distance / 111300
    u = np.random.uniform(0, 1)
    v = np.random.uniform(0, 1)
    w = r * np.sqrt(u)
    t = 2 * np.pi * v
    x = w * np.cos(t)
    x1 = x / np.cos(y0)
    y = w * np.sin(t)
    return x0 + x1, y0 + y


if __name__ == "__main__":
    crear_negocio()

import os
from datetime import datetime

from django.db import models
from django.db.models import Avg
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _
from geoposition.fields import GeopositionField
from imagekit.models import ProcessedImageField
from pilkit.processors import ResizeToFit


def galeria_negocio_upload_to(instance, filename):
    return "galerias/{nombre}/{file}".format(nombre=instance.negocio.pk, file=filename)


class CategoriasNegocio(models.Model):
    nombre_publico = models.CharField(max_length=120)
    descripcion = models.TextField(blank=True)

    def __str__(self):
        return self.nombre_publico

    class Meta:
        verbose_name = 'Categoria de Negocio'
        verbose_name_plural = 'Categorias de Negocios'


class Negocio(models.Model):
    usuario = models.ForeignKey('usuarios.Usuario')
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True)
    categorias = models.ManyToManyField(CategoriasNegocio)
    ubicacion = GeopositionField(null=True)
    direccion = models.CharField(max_length=256, null=True, blank=True)
    fecha_registro = models.DateTimeField(default=datetime.now)
    slug = models.SlugField(editable=False, max_length=60)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Negocio'
        verbose_name_plural = 'Negocios'

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.nombre + "-" + str(self.fecha_registro.date()))
        super(Negocio, self).save(*args, **kwargs)

    @property
    def get_cover_image_url(self):
        if self.imagennegocio_set.filter(is_cover_image=True).count() > 0:
            return self.imagennegocio_set.filter(is_cover_image=True)[0].get_image_url
        elif self.imagennegocio_set.all().count() > 0:
            return self.imagennegocio_set.all()[0].get_image_url
        else:
            from django.contrib.staticfiles.templatetags.staticfiles import static
            return static('base/img/default.png')

    @property
    def get_cover_image(self):
        if self.imagennegocio_set.filter(is_cover_image=True).count() > 0:
            return self.imagennegocio_set.filter(is_cover_image=True)[0]
        elif self.imagennegocio_set.all().count() > 0:
            return self.imagennegocio_set.all()[0]
        else:
            return None

    @property
    def get_dias_laborales(self):
        # not work in MySQL
        return self.horariosemanalnegocio_set.distinct('dia_semana')
        # return self.horariosemanalnegocio_set.values_list('dia_semana', flat=True).distinct()

    @property
    def get_rating(self):
        if self.comentariosnegocio_set.count():
            return round(next(iter(self.comentariosnegocio_set.aggregate(Avg('calificacion')).values())), 1)
        else:
            return 0

    @property
    def tiene_suscripcion(self):
        return self.usuario.tiene_suscripcion


class HorarioSemanalNegocio(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)

    DIAS_SEMANA = (
        ('0', _(u'Domingo')),
        ('1', _(u'Lunes')),
        ('2', _(u'Martes')),
        ('3', _(u'Miércoles')),
        ('4', _(u'Jueves')),
        ('5', _(u'Viernes')),
        ('6', _(u'Sábado')),
    )

    dia_semana = models.CharField(choices=DIAS_SEMANA, max_length=2)
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()

    nota = models.TextField(blank=True)

    def __str__(self):
        return self.negocio.nombre

    class Meta:
        ordering = ['dia_semana']
        verbose_name = 'Horario de Negocio'
        verbose_name_plural = 'Horarios de Negocios'


class ContactoNegocio(models.Model):
    negocio = models.OneToOneField(Negocio, null=True, on_delete=models.CASCADE)
    web_page = models.CharField(max_length=500, blank=True)
    facebook_page = models.CharField(max_length=500, blank=True)
    twitter_page = models.CharField(max_length=500, blank=True)
    telefono = models.CharField(max_length=18, blank=True)

    def __str__(self):
        return self.negocio.nombre

    class Meta:
        verbose_name = 'Contacto de Negocio'
        verbose_name_plural = 'Contacto de negocioes'


class ImagenNegocio(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)
    imagen = ProcessedImageField(upload_to=galeria_negocio_upload_to,
                                 format='JPEG',
                                 processors=[ResizeToFit(1920, 1080)],
                                 options={'quality': 80})

    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True)
    # fecha_modificacion = models.DateTimeField(auto_now=True)
    is_cover_image = models.BooleanField(default=False)

    def __str__(self):
        return self.negocio.nombre + ': ' + os.path.basename(self.imagen.name)

    class Meta:
        verbose_name = 'Imagen de Negocio'
        verbose_name_plural = 'Imágenes de Negocios'

    def save(self, *args, **kwargs):
        if self.is_cover_image:
            other_cover_image = ImagenNegocio.objects.filter(negocio=self.negocio).filter(is_cover_image=True)
            for image in other_cover_image:
                image.is_cover_image = False
                image.save()

        super(ImagenNegocio, self).save()

    @property
    def get_image_filename(self):
        return os.path.basename(self.imagen.name)

    @property
    def get_image_url(self):
        if self.imagen:
            return self.imagen.url
        else:
            from django.contrib.staticfiles.templatetags.staticfiles import static
            return static('base/img/default.png')


class ComentariosNegocio(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)
    usuario = models.ForeignKey('usuarios.Usuario')
    texto = models.TextField()
    CALIFICACIONES = ((5, '5'), (4, '4'), (3, '3'), (2, '2'), (1, '1'))
    calificacion = models.IntegerField(choices=CALIFICACIONES, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    aprobado = models.BooleanField(default=True)

    def __str__(self):
        return self.negocio.nombre + ': ' + str(self.calificacion)

    def aprobar(self):
        self.aprobado = True
        self.save()

    class Meta:
        verbose_name = 'Comentario de Negocio'
        verbose_name_plural = 'Comentarios de Negocios'
        ordering = ["-fecha_creacion"]


class ServiciosNegocio(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)
    nombre_publico = models.CharField(max_length=120)
    descripcion = models.TextField(blank=True)
    precio = models.DecimalField(decimal_places=2, max_digits=6, null=True, blank=True)
    TIPOS_PRECIO = (('1', 'Desde'), ('2', 'Fijo'))
    tipo_precio = models.CharField(max_length=2, choices=TIPOS_PRECIO, default='1')
    tiempo_aprox_minutos = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.negocio.nombre + ': ' + self.nombre_publico

    class Meta:
        verbose_name = 'Servicio de Negocio'
        verbose_name_plural = 'Servicios de Negocios'


class Evento(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE, null=True)
    titulo = models.CharField(max_length=256)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField(null=True)
    all_day = models.BooleanField(default=False)
    notas = models.TextField(blank=True)
    color = models.CharField(max_length=10)

    def __str__(self):
        return self.negocio.nombre + ': ' + str(self.fecha_inicio)

    class Meta:
        verbose_name = 'Evento de Negocio'
        verbose_name_plural = 'Eventos de Negocios'


class SolicitudEvento(models.Model):
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE, null=True)
    mascota = models.ForeignKey('mascotas.Mascota', null=True)
    servicio = models.ForeignKey(ServiciosNegocio, blank=True, null=True)
    evento = models.OneToOneField(Evento, null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True)
    fecha = models.DateTimeField()
    ESTADOS_SOLICITUD = (('1', 'Pendiente'), ('2', 'Aceptado'), ('3', 'Cancelado'))
    estado = models.CharField(max_length=2, choices=ESTADOS_SOLICITUD, default='1')

    def __str__(self):
        return self.negocio.nombre + ': ' + str(self.fecha)

    class Meta:
        verbose_name = 'Solicitud de Cita'
        verbose_name_plural = 'Solicitudes de Cita'


# Receive the pre_delete signal and delete the file associated with the model instance.
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


@receiver(pre_delete, sender=ImagenNegocio)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.imagen.delete(False)

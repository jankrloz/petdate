from django import forms
from django.forms import BaseModelFormSet

from .models import Negocio, CategoriasNegocio, ContactoNegocio, HorarioSemanalNegocio, ServiciosNegocio, ImagenNegocio, \
    ComentariosNegocio


class NegocioForm(forms.ModelForm):
    categorias = forms.ModelMultipleChoiceField(required=True,
                                                queryset=CategoriasNegocio.objects.all().order_by('nombre_publico'))

    class Meta:
        model = Negocio
        fields = ['categorias', 'nombre', 'descripcion', 'ubicacion']
        widgets = {
            'nombre': forms.TextInput(attrs={
                'required': True,
                'minlength': 4,
                'class': 'validate'
            }),
            'categorias': forms.CheckboxSelectMultiple(attrs={
                'required': True,
                'class': 'validate'
            }),
            'descripcion': forms.Textarea(attrs={
                'class': 'materialize-textarea validate',
                'required': True,
                'minlength': 18,
                'maxlength': 140,
                'length': 140,
            }),
        }
        labels = {
            'nombre': 'Nombre',
            'descripcion': 'Descripción',
            'categorias': 'Categorías',
            'ubicacion': ''
        }


class ContactoNegocioForm(forms.ModelForm):
    class Meta:
        model = ContactoNegocio
        fields = ['web_page', 'facebook_page', 'twitter_page', 'telefono']
        labels = {
            'web_page': 'Página web',
            'facebook_page': 'Página de Facebook',
            'twitter_page': 'Página de Twitter',
            'telefono': 'Teléfono'
        }


class HorarioNegocioForm(forms.ModelForm):
    class Meta:
        model = HorarioSemanalNegocio
        fields = ('dia_semana', 'hora_inicio', 'hora_fin')

        widgets = {
            'dia_semana': forms.Select(attrs={
                'class': 'select-dia-semana',
                'required': True
            }),
            'hora_inicio': forms.TimeInput(format='%I:%M %p', attrs={
                'class': 'timepicker',
                'required': True
            }),
            'hora_fin': forms.TimeInput(format='%I:%M %p', attrs={
                'class': 'timepicker',
                'required': True
            })
        }

        labels = {
            'dia_semana': '',
        }


class ServiciosNegocioForm(forms.ModelForm):
    class Meta:
        model = ServiciosNegocio
        fields = ('nombre_publico', 'descripcion', 'tipo_precio', 'precio', 'tiempo_aprox_minutos')

        widgets = {
            'nombre_publico': forms.TextInput(attrs={
                'required': True
            }),
            'descripcion': forms.Textarea(attrs={
                'class': 'materialize-textarea',
                'required': True,
                'minlength': 18,
                'maxlength': 280,
            }),
            'tipo_precio': forms.Select(attrs={
                'required': True
            }),
            'precio': forms.TextInput(attrs={
                'required': False
            }),
            'tiempo_aprox_minutos': forms.TextInput(attrs={
                'required': False
            }),
        }

        labels = {
            'nombre_publico': 'Nombre del servicio',
            'descripcion': 'Descripción',
            'tipo_precio': 'Tipo de precio',
            'precio': 'Precio',
            'tiempo_aprox_minutos': 'Duración (minutos)',
        }


class GaleriaNegocioForm(forms.ModelForm):
    class Meta:
        model = ImagenNegocio
        fields = ('imagen', 'is_cover_image')
        labels = {
            'is_cover_image': '¿Foto principal?'
        }


class BaseHorarioFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.negocio = kwargs.pop('negocio')
        super(BaseHorarioFormSet, self).__init__(*args, **kwargs)
        self.queryset = HorarioSemanalNegocio.objects.filter(negocio=self.negocio)


class BaseServiciosFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.negocio = kwargs.pop('negocio')
        super(BaseServiciosFormSet, self).__init__(*args, **kwargs)
        self.queryset = ServiciosNegocio.objects.filter(negocio=self.negocio)


class BaseGaleriaFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.negocio = kwargs.pop('negocio')
        super(BaseGaleriaFormSet, self).__init__(*args, **kwargs)
        self.queryset = ImagenNegocio.objects.filter(negocio=self.negocio)


class ComentariosNegocioForm(forms.ModelForm):
    class Meta:
        model = ComentariosNegocio
        fields = ['calificacion', "texto"]

        widgets = {
            'calificacion': forms.RadioSelect(attrs={
                'required': True
            }),
            "texto": forms.Textarea(attrs={
                'class': 'materialize-textarea',
                'required': True,
            }),
        }

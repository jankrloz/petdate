# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-12 17:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('negocios', '0030_auto_20161009_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='suscripcionnegocio',
            name='prueba',
            field=models.BooleanField(default=False),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-08 21:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('negocios', '0017_auto_20160805_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviciosnegocio',
            name='precio',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True),
        ),
    ]

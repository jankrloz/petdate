from django.contrib import admin

# Register your models here.
from apps.pagos.models import PagosPayPal


@admin.register(PagosPayPal)
class PayPalAdmin(admin.ModelAdmin):
    pass

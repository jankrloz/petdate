from django.db import models

# Create your models here.
from apps.usuarios.models import Usuario


class PagosPayPal(models.Model):
    usuario = models.ForeignKey(Usuario)
    fecha = models.DateTimeField(auto_now_add=True)
    paypal_id = models.CharField(max_length=200, default="")
    ajax_peticion = models.TextField(max_length=3000, default="")
    ajax_autorizacion = models.TextField(max_length=3000, default="")
    ajax_confirmacion = models.TextField(max_length=3000, default="")
    estado = models.CharField(max_length=30)

    def __str__(self):
        return str(self.usuario.get_full_name()) + " pago en la fecha" + str(self.fecha) + " - " + str(self.estado)

    class Meta:
        verbose_name = 'Pagos PayPal'
        verbose_name_plural = 'Pagos PayPal'

import logging
from datetime import date

import paypalrestsdk
from dateutil.relativedelta import relativedelta

from apps.pagos.models import PagosPayPal
from apps.usuarios.models import SuscripcionUsuario

miConf = paypalrestsdk.Api(
        {
            "mode": "sandbox",  # sandbox or live
            "client_id": "AXiulhLO30qEsxbP22uCGDhMmrMbdAyT1dx8OwFd38l_RKwfh2LbE1lF6f2eYt936CpIqE3xsfnPh53m",
            "client_secret": "EPhvbTpXIgCgGrX2YIk9-wqme8OVxtgvM8iau_K5L_qrkunao6kLWCFpswJg3EC9cD02IleP3zNzWseb"
        }
)


def iniciar_configuracio():
    try:
        paypalrestsdk.configure(
                {
                    "mode": "sandbox",  # sandbox or live
                    "client_id": "AXiulhLO30qEsxbP22uCGDhMmrMbdAyT1dx8OwFd38l_RKwfh2LbE1lF6f2eYt936CpIqE3xsfnPh53m",
                    "client_secret": "EPhvbTpXIgCgGrX2YIk9-wqme8OVxtgvM8iau_K5L_qrkunao6kLWCFpswJg3EC9cD02IleP3zNzWseb"
                }
        )
    except Exception as e:
        print("no se pudo iniciar la api por: ")
        print(e.args)


def crear_pago_paypal():
    iniciar_configuracio()
    logging.basicConfig(level=logging.INFO)
    direccion = ""
    id_payment = ""
    try:
        payment = paypalrestsdk.Payment({
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"},
            "redirect_urls": {
                "return_url": "192.168.1.88:8000/pagos/completado",
                "cancel_url": "192.168.1.88:8000/test"},

            "transactions": [{
                "amount": {
                    "total": "320",
                    "currency": "MXN"},
                "description": "Suscripcion Mi PetDate"}]})

        if payment.create():
            id_payment = payment.id
            for link in payment.links:
                if link.method == 'REDIRECT':
                    direccion = link.href
        print(id_payment)
        print(direccion)
    except Exception as e:
        print(e.args)
    return id_payment, direccion


def otracosa(payment):
    redireccion = ""
    pagado = None
    # Create Payment and return status
    if payment.create():
        pagado = True
        print("Payment[%s] created successfully" % (payment.id))
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                # Convert to str to avoid google appengine unicode issue
                # https://github.com/paypal/rest-api-sdk-python/pull/58
                redirect_url = str(link.href)
                redireccion = redirect_url
                print("Redirect for approval: %s" % (redirect_url))
    else:
        pagado = False
        print("Error while creating payment:")
        print(payment.error)
    return pagado, redireccion


def revisar_pagos_pendientes():
    pagos_pendientes = PagosPayPal.objects.filter(estado="pending")
    paypalrestsdk.configure(
            {
                "mode": "sandbox",  # sandbox or live
                "client_id": "AXiulhLO30qEsxbP22uCGDhMmrMbdAyT1dx8OwFd38l_RKwfh2LbE1lF6f2eYt936CpIqE3xsfnPh53m",
                "client_secret": "EPhvbTpXIgCgGrX2YIk9-wqme8OVxtgvM8iau_K5L_qrkunao6kLWCFpswJg3EC9cD02IleP3zNzWseb"
            }
    )
    for pago in pagos_pendientes:
        payment = paypalrestsdk.Payment.find(pago.paypal_id)
        estado = payment["transactions"][0]["related_resources"][0]["sale"]["state"]
        pago.estado = estado
        pago.ajax_confirmacion = payment.__str__()
        pago.save()
        if estado == "completed":
            SuscripcionUsuario.objects.update_or_create(
                    usuario=pago.usuario, defaults={
                    "fecha_limite": date.today() + relativedelta(days=30),
                    "estado": '1'
                    }
            )

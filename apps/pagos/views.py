from datetime import date

import paypalrestsdk
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect

from apps.pagos.models import PagosPayPal
from apps.usuarios.correos import enviar_correo
from apps.usuarios.models import SuscripcionUsuario


@login_required
def pago_suscripcion(request):
    return render(request, "pagos/testPagos.html", {

    })


def completado(request):
    return render(request, "pagos/testcompleto.html")


def verificacion(request):
    '''
    print("inicia el post")
    print(request.POST)
    print("inicia el get")
    print(request.GET)
    print(request.session["paypal_id"])
    '''
    if "PayerID" in request.GET and "paymentId" in request.GET and "token" in request.GET:
        # print(request.GET["paymentId"])
        # print(request.session["paypal_id"])
        if request.GET["paymentId"] == request.session["paypal_id"]:
            try:
                paypalrestsdk.configure(
                        {
                            "mode": "sandbox",  # sandbox or live
                            "client_id": "AXiulhLO30qEsxbP22uCGDhMmrMbdAyT1dx8OwFd38l_RKwfh2LbE1lF6f2eYt936CpIqE3xsfnPh53m",
                            "client_secret": "EPhvbTpXIgCgGrX2YIk9-wqme8OVxtgvM8iau_K5L_qrkunao6kLWCFpswJg3EC9cD02IleP3zNzWseb"
                        }
                )
                payment = paypalrestsdk.Payment.find(request.GET["paymentId"])
                payment.execute({"payer_id": request.GET["PayerID"]})
                pago = PagosPayPal.objects.get(paypal_id=request.GET["paymentId"])
                print(payment)
                try:
                    estado = payment["transactions"][0]["related_resources"][0]["sale"]["state"]
                    pago.estado = estado
                    pago.ajax_confirmacion = payment.__str__()
                    pago.save()
                    SuscripcionUsuario.objects.update_or_create(
                            usuario=request.user, defaults={
                            "fecha_limite": date.today() + relativedelta(days=30),
                            "estado": '1'
                            }
                    )
                    enviar_correo(destino=request.user.email,
                                  asunto='¡Suscripción mipetdate.com autorizada!',
                                  template='suscripcion')
                    if estado == "completed":
                        print("el pago esta completado")
                    if estado == "pending":
                        print("el pago esta pendiente")
                    return redirect(reverse('pagos_app:PagoCompleto'))
                except Exception as e:
                    print(e.args)
                    print("no se pudo ejecutar el pago")
            except Exception as e:
                print(e.args)
    return redirect(reverse('negocios_app:negocios_usuario'))


def ajax_crear_pago(request):
    '''
    print("inicia el post")
    print(request.POST)
    print("inicia el get")
    print(request.GET)
    '''
    if request.is_ajax():
        if request.user.is_authenticated():
            try:
                # id_payment = ""
                paypalrestsdk.configure(
                        {
                            "mode": "sandbox",  # sandbox or live
                            "client_id": "AXiulhLO30qEsxbP22uCGDhMmrMbdAyT1dx8OwFd38l_RKwfh2LbE1lF6f2eYt936CpIqE3xsfnPh53m",
                            "client_secret": "EPhvbTpXIgCgGrX2YIk9-wqme8OVxtgvM8iau_K5L_qrkunao6kLWCFpswJg3EC9cD02IleP3zNzWseb"
                        }
                )
                payment = paypalrestsdk.Payment({
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"},
                    "redirect_urls": {
                        "return_url": "https://mipetdate.com" + reverse('pagos_app:Verificacion'),
                        "cancel_url": "https://mipetdate.com" + reverse('negocios_app:negocios_usuario')},
                    "transactions": [{
                        "amount": {
                            "total": "320",
                            "currency": "MXN"},
                        "description": "Suscripcion Mi PetDate"}]})
                pago = PagosPayPal.objects.create(
                        usuario=request.user,
                        ajax_peticion=payment.__str__()
                )
                if payment.create():
                    id_payment = payment.id
                    request.session["paypal_id"] = id_payment
                    pago.paypal_id = id_payment
                    pago.estado = "sending"
                    pago.save()
                    for link in payment.links:
                        if link.method == 'REDIRECT':
                            pago.ajax_autorizacion = payment.__str__()
                            pago.estado = "created"
                            pago.save()
                            '''
                            direccion = link.href
                            print(direccion)
                            print(id_payment)
                            print(payment)
                            '''
                            return HttpResponse(link.href, status=200)
                else:
                    return HttpResponse("no paso el cargo", status=500)
            except Exception as e:
                return HttpResponse(e.args, status=500)
            return HttpResponse("No se pudo completar el pago intente mas tarde", status=500)
        else:
            return HttpResponse("Intentando entrar sin usuario", status=500)
    return HttpResponse("Intentando entrar sin ajax", status=500)

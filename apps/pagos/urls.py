# coding=utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^pagos/test/$', "apps.pagos.views.pago_suscripcion", name='testPago'),
    url(r'^pagos/verificar/$', "apps.pagos.views.verificacion", name='Verificacion'),
    url(r'^pagos/completado/$', "apps.pagos.views.completado", name='PagoCompleto'),
    url(r'^pagos/api/crear$', "apps.pagos.views.ajax_crear_pago", name='ajax_crear_pago'),
]

# coding=utf-8
import dj_database_url

from .base import *

ON_HEROKU = os.environ.get('ON_HEROKU')

DEBUG = False

ALLOWED_HOSTS = ['localhost', '0.0.0.0', '127.0.0.1', 'petdate.herokuapp.com', 'mipetdate.com', 'web.mipetdate.com']

ADMINS = (
    ('admin', 'jankrloz.navarrete@gmail.com'),
)

SOCIAL_AUTH_FACEBOOK_KEY = '165223117273560'
SOCIAL_AUTH_FACEBOOK_SECRET = 'f41627a679fa6bc2de2d83c4d4b5adc0'

if ON_HEROKU:
    db_from_env = dj_database_url.config(conn_max_age=500)
    DATABASES = {'default': db_from_env}

else:
    DATABASE_URL = 'postgres://fqkkrbudjogqmi:9yqC1SELmQ_NzDRsI4OfGOGfcj@ec2-54-225-81-90.compute-1.amazonaws.com:5432/d4q3l7c0e6gs4b'
    db_from_env = dj_database_url.config(conn_max_age=500, default=DATABASE_URL)
    DATABASES = {'default': db_from_env}

# coding=utf-8
import dj_database_url

from .base import *

DEBUG = True
ALLOWED_HOSTS = ['localhost', '0.0.0.0']

SOCIAL_AUTH_FACEBOOK_KEY = '165245217271350'
SOCIAL_AUTH_FACEBOOK_SECRET = 'cbd4c505a1507c5f482da4036cdbd80c'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'petdate',
        'USER': 'jankrloz',
        'PASSWORD': '9212',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles import views

urlpatterns = [

    # Python Social Auth URLs
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^users/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="user-logout"),

    url(r'^', include('apps.base.urls', namespace='base_app')),
    url(r'^', include('apps.usuarios.urls', namespace='usuarios_app')),
    url(r'^', include('apps.negocios.urls', namespace='negocios_app')),
    url(r'^', include('apps.mascotas.urls', namespace='mascotas_app')),
    url(r'^', include('apps.publicidad.urls', namespace='publicidad_app')),
    url(r'^', include('apps.pagos.urls', namespace='pagos_app')),

    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
